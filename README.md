# Le radio di mastodon 
Ascolare la musica su mastodon è molto facile, oltre alle proposte musicali di [Funkwhale](https://funkwhale.it) e di mastodon seguendo l'hashtag [Mastoradio](https://mastodon.uno/tags/mastoradio) esistono 2 radio che permettono di ascoltare ininterrottamente la musica selezionata dagli utenti di mastodon: [Unitoo Web Radio](https://radio.unitoo.it/public/liveradio) che è gestita da speaker e la radio  [Eldricht Radio Mastodon](https://radio.eldritch.cafe/) che pesca in automatico la musica pubblicata sulle istanze mastodon.


## Eldritch Radio Mastodon

[Questa radio](https://radio.eldritch.cafe/) è alimentata dalle pubblicazioni fatte sulla piattaforma di social media [Mastodon](https://mastodon.it). L'obiettivo è quello di creare un luogo per scoprire la musica al di fuori della vostra solita bolla. Spero che vi piaccia!

### Da dove viene la musica?
L'app prende tutte le pubblicazioni pubbliche che contengono un link ai media e un hashtag musicale (puoi configurarlo nelle Impostazioni di solito viene usato mastoradio). Per ora supportiamo solo i link di Youtube, ma altre piattaforme sono previste in futuro.
Non possediamo i diritti musicali quindi le canzoni vengono riprodotte sul player della piattaforma originale.

### Come posso far suonare la mia canzone alla radio?
Devi fare un post pubblico contenente uno degli hashtag configurati sull'app e un link a Youtube.

### Posso usare la radio sul mio telefono?
È una webapp quindi dovrebbe funzionare, tuttavia poiché il player di Youtube ci permette di riprodurre una canzone solo quando sei sul web e si ferma quando lo schermo si spegne o quando sei su un'altra app, è un po' inutilizzabile.
È un po' una merda, ma finché Youtube ha questo comportamento non vediamo un modo per fare una grande esperienza per la radio sui dispositivi mobili.

### Come posso contattarvi? Ho rilevato un problema, ho un'idea o un messaggio gentile.
Se sei uno sviluppatore puoi creare un problema qui su gitea. Per qualsiasi altra cosa puoi contattarci su [Mastodon](https://mastodon.uno/@mastodon).

## Unitoo Web Radio 

[Una radio](https://radio.unitoo.it/public/liveradio) con contenuti esclusivamente licenziati Creative Commons che tratta software libero. 
Tanti programmi settimanali sono già in riproduzione e tanti altri in fase di realizzazione grazie agli speaker freelance.
I brani in onda su questa WebRadio sono rilasciati sotto licenze Creative Commons (CC) - copyleft, di pubblico dominio o di artisti / etichette non iscritti a SIAE od SCF, che hanno rilasciato apposite liberatorie. 

## Project setup
```
npm i
```
or if you don't want to impact the package-lock
```
npm ci
```

### Compiles and hot-reloads for development
```
npm run dev
```

### Compiles and minifies for production
```
npm run build
```

### License

The project is under [Anti-Fascist MIT License](https://github.com/Laurelai/anti-fascist-mit-license)